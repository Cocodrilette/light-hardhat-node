FROM node:18-alpine

WORKDIR usr/src/apps/node

COPY package*.json ./

RUN npm install -g pnpm && pnpm install

COPY . .

CMD npx hardhat node --hostname 0.0.0.0