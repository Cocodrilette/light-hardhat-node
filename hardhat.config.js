require("dotenv").config();

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.19",
  networks: {
    hardhat: {
      mining: {
        auto: true,
        interval: [10000, 20000],
      },
      accounts: {
        count: 50,
        accountsBalance: (100 * 10 ** 18).toString(),
      },
    },
  },
};
